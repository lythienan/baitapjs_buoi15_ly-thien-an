function tinhTienDien() {
  var hoTen = document.getElementById("txt-ho-ten").value;
  var kW = document.getElementById("txt-kw").value * 1;

  var soTien50KwDau = 500;
  var soTien50KwSau = 650;
  var soTien100KwKe = 850;
  var soTien150KwKe = 1100;
  var tu150KwTroDi = 1300;
  

  var tongTien = 0;

  if (kW >= 0 && kW <= 50) {
    tongTien = kW * soTien50KwDau;
  } else if (kW >= 51 && kW <= 100) {
    tongTien = 50 * soTien50KwDau + (kW - 50) * soTien50KwSau;
  } else if (kW >= 101 && kW <= 200) {
    tongTien =
      50 * soTien50KwDau + 50 * soTien50KwSau + (kW - 100) * soTien100KwKe;
  } else if (kW >= 201 && kW <= 350) {
    tongTien =
      50 * soTien50KwDau +
      50 * soTien50KwSau +
      100 * soTien100KwKe +
      (kW - 200) * soTien150KwKe;
  } else {
    tongTien =
      50 * soTien50KwDau +
      50 * soTien50KwSau +
      100 * soTien100KwKe +
      150 * soTien150KwKe +
      (kW - 350) * tu150KwTroDi;
      
  }

  document.getElementById(
    "inRaManHinh"
  ).innerHTML = `Họ Tên: ${hoTen} ; Tiền Điện: ${tongTien}`;
}
